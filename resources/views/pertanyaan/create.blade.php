@extends('master')

@section('content')
			<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Buat Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan" method="POST">
							@csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Judul Pertanyaan</label>
                    <input type="text" class="form-control" id="judul-pertanyaan" name="judul-pertanyaan" placeholder="Enter pertanyaan">
                  </div>
                  <div class="form-group">
                    <label for="body">Isi Pertanyaan</label>
                    <input type="text" class="form-control" id="isi-pertanyaan" name="isi-pertanyaan"placeholder="body pertanyaan">
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
@endsection