@extends('master')

@section('content')
			<div class="card">
              <div class="card-header">
                <h3 class="card-title">Pertanyaan Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul Pertayaan</th>
                      <th>Isi Pertanyaan</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
									@foreach($pertanyaan as $key => $value)
										 <tr>
												 <td> {{ $key + 1 }} </td>
												 <td> {{ $value->judul pertanyaan }} </td>
												 <td> {{ $value->isi pertanyaan }} </td>
												 <td>action</td>
										 </tr>
                  @endforeach
                    <tr>
                      <td>1.</td>
                      <td>Update software</td>
                      <td>
                        <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                        </div>
                      </td>
                      <td><span class="badge bg-danger">55%</span></td>
                    </tr>

                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->

            </div>
					</div>
@endsection