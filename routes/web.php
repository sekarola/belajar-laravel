<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
		return view('index');
});

Route::get('/form', function () {
		return view('form');
});

Route::get('/last', function () {
		return view('last');
});

Route::get('/items', function () {
		 return view('items.index');
});

Route::get('/index', 'RegisterController@index');

Route::get('/form', 'RegisterController@form');

Route::get('/last', 'RegisterController@last');
Route::post('/last', 'RegisterController@last_post');

Route::get('/master', function() {
			 return view('master');
});

Route::get('/items', function() {
				return view('items.index');
});

Route::get('/items/tabel', function() {
		    return view('items.tabel');
});

Route::get('/pertanyaan', 'pertanyaanController@index');
Route::get('/pertanyaan/create', 'pertanyaanController@create');
Route::post('/pertanyaan', 'pertanyaanController@store');
