<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class pertanyaanController extends Controller
{
	 public function index() {
			 $pertanyaan = DB::table('pertanyaan')->get();
			 //dd($pertanyaan);
			 return view('pertanyaan.index', compact('pertanyaan'));
	 }

		public function create() {
			 return view('pertanyaan.create');
		}

    public function store(Request $request) {
				 //dd($request->all());
				 $query = DB::table('pertanyaan')->insert([
						 "judul" => $request['judul-pertanyaan'],
						 "isi"=> $request['isi-pertanyaan'],
						 "tanggal_dibuat"=> "2021-03-05",
						 "tanggal_diperbaharui"=>"2021-03-05",
						 "profil_id"=> "NULL",
						 "jawaban_tepat_id"=> "NULL"
				 ]);
				 return redirect('pertanyaan/create');
		}
}