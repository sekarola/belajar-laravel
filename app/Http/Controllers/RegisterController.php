<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index() {
			return view('index');
		}

     public function form() {
			return view('form');
		}

		public function last(Request $request)
		{
		   //dd($request->all());
			$nama = $request->first_name;
			$okey = $request->last_name;
			return view ('last', compact('nama','okey'));
		}

}